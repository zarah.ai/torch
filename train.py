#!/usr/bin/env python3
from __future__ import absolute_import, division, print_function, unicode_literals
from os import makedirs
from os.path import isdir, dirname
from torch import device, save, no_grad, tensor, float32
from torch.nn import CrossEntropyLoss
from torch.cuda import is_available as has_cuda
from torch.optim import Adam
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.nn.utils import clip_grad_norm_
from model import Transformer
from bar import print_bar
from data import load_dataframe
from vocab import load_vocab
from time import time

def train(filepath, learning_rate, iterations, batch_size, outpath):
    d = device("cuda" if has_cuda() else "cpu")
    print(f"Training model on {d}")

    train_data = load_dataframe(f"{filepath}/train.json").sampler(batch_size)
    valid_data = load_dataframe(f"{filepath}/valid.json").sampler(batch_size)
    vocab = load_vocab(f"{filepath}/vocab.json")

    if not isdir(outpath):
        makedirs(outpath)

    ncheckpoints = max(10, iterations // 100)
    train_steps = iterations // ncheckpoints
    valid_steps = max(train_steps // 10, 1)

    model = Transformer(len(vocab)).to(d)
    loss = CrossEntropyLoss(ignore_index=0).to(d)
    optimizer = Adam(model.parameters(), lr=learning_rate)
    scheduler = ReduceLROnPlateau(optimizer, patience=1)

    for n in range(ncheckpoints):
        start_time = time()
        lr = optimizer.param_groups[0]["lr"]
        print(f"Checkpoint {n+1}/{ncheckpoints} [lr: {'%.0e' % lr}]")
        train_loss = run_train(model, train_steps, train_data, optimizer, loss, d)
        valid_loss = run_valid(model, valid_steps, valid_data, loss, d)
        save(model.state_dict(), f"{outpath}/model.pt")
        scheduler.step(valid_loss)
        elapsed_time = int(time() - start_time)
        print(f"Checkpoint completed in {elapsed_time}s [d: {'%.3f' % (train_loss - valid_loss)}]")

def run_train(model, steps, data, optimizer, loss, d):
    model.train()
    total_loss = tensor(0, device=d, dtype=float32)
    previous_length = print_bar(0, steps, suffix="Starting training")
    for k in range(steps):
        total_loss += run_train_step(data.next(d), model, optimizer, loss)
        average_loss = total_loss / (k+1)
        previous_length = print_bar(k+1, steps, suffix=f"loss: {'%.3f' % average_loss}", previous_length=previous_length)
    return average_loss

def run_valid(model, steps, data, loss, d):
    model.eval()
    total_loss = tensor(0, device=d, dtype=float32)
    previous_length = print_bar(0, steps, suffix="Starting validation")
    for k in range(steps):
        total_loss += run_valid_step(data.next(d), model, loss)
        average_loss = total_loss / (k+1)
        previous_length = print_bar(k+1, steps, suffix=f"val_loss: {'%.3f' % average_loss}", previous_length=previous_length)
    return average_loss

def run_train_step(batch, model, optimizer, loss):
    optimizer.zero_grad()
    pred = model(batch[0], batch[1], batch[2][:-1], batch[3][:, :-1], batch[4][:-1, :-1]).flatten(0, 1)
    true = batch[2][1:].flatten()
    loss_value = loss(pred, true)
    loss_value.backward()
    clip_grad_norm_(model.parameters(), 0.5)
    optimizer.step()
    return loss_value

def run_valid_step(batch, model, loss):
    with no_grad():
        pred = model(batch[0], batch[1], batch[2][:-1], batch[3][:, :-1], batch[4][:-1, :-1]).flatten(0, 1)
        true = batch[2][1:].flatten()
        loss_value = loss(pred, true)
    return loss_value

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("--folder", default="tmp/data", help="specify where to find the dataset")
    parser.add_argument("--outpath", default="tmp", help="specify where to find the dataset")
    parser.add_argument("--learning-rate", default=1e-3, type=float)
    parser.add_argument("--iterations", default=5000, type=int)
    parser.add_argument("--batch-size", default=128, type=int)
    args = parser.parse_args()

    train(args.folder, args.learning_rate, args.iterations, args.batch_size, args.outpath)
