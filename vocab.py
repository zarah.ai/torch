#!/usr/bin/env python3
from json import load, dump
from re import sub
from unicodedata import normalize, category

class Vocab:
    def __init__(self, sentences=[]):
        sentences = [ self._normalize(sentence) for sentence in sentences ]
        words = [ item for list in sentences for item in list ]
        self.word2index = { "PAD": 0, "SOS": 1, "EOS": 2  }
        for word in words:
            if not word in self.word2index.keys():
                self.word2index[word] = len(self.word2index)

        self.index2word = { v: k for k, v in self.word2index.items() }

    def __len__(self):
        return len(self.word2index)

    def _normalize(self, sentence):
        sentence = sentence.lower().strip()
        sentence = "".join([ c for c in normalize("NFD", sentence) if category(c) != "Mn" ])
        sentence = sub(r"([.!?])", r" \1", sentence)
        sentence = sub(r"[^a-zA-Z.!?]+", r" ", sentence)
        sentence = sub(r"\s+", r" ", sentence).strip()
        return sentence.split()

    def encode(self, sentence):
        if isinstance(sentence, list):
            return [ self.encode(part) for part in sentence ]
        sentence = self._normalize(sentence)
        sentence = [ self.word2index[word] if word in self.word2index.keys() else 0 for word in sentence ]
        return sentence

    def decode(self, sentence):
        if any(isinstance(el, list) for el in sentence):
            return [ self.decode(part) for part in sentence ]
        sentence = [ self.index2word[word] if word in self.index2word.keys() else "PAD" for word in sentence ]
        start = sentence.index("SOS") if "SOS" in sentence else -1
        end = sentence.index("EOS") if "EOS" in sentence else -1
        return sentence[start+1:end]

    def save(self, filename):
        with open(filename, "w+") as f:
            dump(self.word2index, f)

def load_vocab(filename):
    with open(filename, "r") as f:
        word2index = load(f)
    vocab = Vocab()
    vocab.word2index = word2index
    vocab.index2word = { v: k for k, v in vocab.word2index.items() }
    return vocab

if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("--vocab", default="tmp/data/vocab.json", help="path to the vocab file")
    parser.add_argument("--test-sentence", default="This is a test sentence withawordidontunderstand?!", help="the sentence to encode and decode")
    args = parser.parse_args()

    vocab = load_vocab(args.vocab)
    normalized = vocab._normalize(args.test_sentence)
    encoded = vocab.encode(args.test_sentence)
    decoded = vocab.decode(encoded)

    print(f"normalized -> {normalized}")
    print(f"encoded    -> {encoded}")
    print(f"decoded    -> {decoded}")
