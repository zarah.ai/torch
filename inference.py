#!/usr/bin/env python3
from model import Transformer
from vocab import load_vocab
from torch import load, device, tensor, cat, ones, triu
from torch.cuda import is_available as has_cuda
from torch.nn.functional import softmax
from torch.distributions.categorical import Categorical


def inference(input, model, vocab, d):
    encoder_input = tensor(vocab.encode(query), device=d).unsqueeze(-1)
    encoder_mask = (encoder_input==0).transpose(0, 1)
    decoder_input = tensor([1], device=d, dtype=int).unsqueeze(-1)
    encoding = model.encode(encoder_input, encoder_mask)
    for n in range(25):
        nopeek = decoder_input.size()[0]
        nopeek = ones(nopeek, nopeek, device=d)
        nopeek = triu(nopeek).transpose(0, 1)
        nopeek = nopeek.masked_fill(nopeek == 0, float('-inf')).masked_fill(nopeek == 1, float(0.0))

        decoder_mask = (decoder_input==0).transpose(0, 1)

        decoding = model.decode(decoder_input, decoder_mask, nopeek, encoding, encoder_mask)
        output = model.linear(decoding)[-1]
        token = softmax(output, dim=-1).argmax(-1).unsqueeze(0)
        decoder_input = cat((decoder_input, token), dim=0)

    response = decoder_input.flatten().tolist()
    response = vocab.decode(response)
    return " ".join(response)


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("--model", default="tmp/model.pt")
    parser.add_argument("--vocab", default="tmp/data/vocab.json")
    args = parser.parse_args()

    d = device("cuda" if has_cuda() else "cpu")
    print(f"Running inference on {d}")

    vocab = load_vocab(args.vocab)
    model = Transformer(len(vocab)).to(d)
    model.load_state_dict(load(args.model, map_location=d))
    model.eval()

    print("Please enter your query:")
    while True:
        query = input("You: ")
        response = inference(query, model, vocab, d)
        print(f"Zarah: {response}")
