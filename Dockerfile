FROM pytorch/pytorch:1.7.1-cuda11.0-cudnn8-runtime
WORKDIR '/workspace'
COPY requirements.txt .
RUN pip3 install -r requirements.txt
COPY . .
RUN chmod +x run
ENTRYPOINT ["/bin/bash", "run"]
