#!/usr/bin/env python3
from math import log, sqrt
from torch import zeros, arange, exp, sin, cos, float32
from torch.nn import Module, Embedding, Linear, Dropout
from torch.nn import TransformerEncoder, TransformerEncoderLayer
from torch.nn import TransformerDecoder, TransformerDecoderLayer
from torch.nn.init import xavier_normal_

class Transformer(Module):
    def __init__(self, ntoken, nembed=256, nhid=512, nhead=8, nlayers=2, dropout=0.1):
        super(Transformer, self).__init__()
        self.embedding = Embedding(ntoken, nembed)
        self.positional = Positional(nembed, dropout)
        encoder_layer = TransformerEncoderLayer(nembed, nhead, nhid, dropout)
        self.encoder = TransformerEncoder(encoder_layer, nlayers)
        decoder_layer = TransformerDecoderLayer(nembed, nhead, nhid, dropout)
        self.decoder = TransformerDecoder(decoder_layer, nlayers)
        self.linear = Linear(nembed, ntoken)

        for p in self.parameters():
            if p.dim() > 1:
                xavier_normal_(p)

    def encode(self, src, scr_msk):
        src = self.embedding(src)
        src = self.positional(src)
        src = self.encoder(src, src_key_padding_mask=scr_msk)
        return src

    def decode(self, tgt, tgt_msk, nopeek_msk, mem, mem_msk):
        tgt = self.embedding(tgt)
        tgt = self.positional(tgt)
        tgt = self.decoder(tgt, mem, tgt_mask=nopeek_msk, tgt_key_padding_mask=tgt_msk, memory_key_padding_mask=mem_msk)
        return tgt

    def forward(self, src, src_msk, tgt, tgt_msk, nopeek_msk):
        enc = self.encode(src, src_msk)
        dec = self.decode(tgt, tgt_msk, nopeek_msk, enc, src_msk)
        return self.linear(dec)

class Positional(Module):
    def __init__(self, nhid, dropout=0.1, max_len=5000):
        super(Positional, self).__init__()
        self.dropout = Dropout(p=dropout)

        pe = zeros(max_len, nhid)
        position = arange(0, max_len, dtype=float32).unsqueeze(1)
        div_term = exp(arange(0, nhid, 2).float() * (-log(10000.0) / nhid))
        pe[:, 0::2] = sin(position * div_term)
        pe[:, 1::2] = cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)
        self.factor = nhid

    def forward(self, input):
        output = input * self.factor + self.pe[:input.size(0), :]
        return self.dropout(output)

if __name__ == "__main__":
    from argparse import ArgumentParser
    from torch import randint, empty

    parser = ArgumentParser()
    parser.add_argument("--vocab-size", default=1024, help="the size of the fictional vocabulary")
    parser.add_argument("--input-length", default=25, help="the max lenght of the input")
    parser.add_argument("--n-layers", default=1, help="the number of encoder and decoder layers")
    parser.add_argument("--hidden-size", default=512, help="the size of the hidden vector")
    parser.add_argument("--batch-size", default=128, help="the size of the mini-batches")
    args = parser.parse_args()

    transformer = TransformerModel(args.vocab_size, args.hidden_size, args.n_layers)
    transformer.eval()
    input = randint(args.vocab_size, [args.batch_size, args.input_length])
    output = transformer(input)
    print(f"{input.size()} -> {output.size()}")
