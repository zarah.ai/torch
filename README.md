### Available Commands

* `./bar.py` - Print a test progress bar.
* `./cornell.py` - Transforms the cornell movie-dialogs corpus into a universal format and vocab.
* `./data.py` - Loads a universal format dataset.
* `./inference.py` - Talk to a model.
* `./model.py` - Runs a test tensor through a the transformer model.
* `./tatoeba.py` - Transforms one of the tatoeba corpuses into a universal format and vocab.
* `./train.py` - Train a chatbot model with torch.
* `./vocab.py` - Loads a universal format vocabulary and encode and decode a string.


### Deploy docker image

```
sudo docker run --name torch registry.gitlab.com/zarah.ai/torch <command> <parameters>
```
