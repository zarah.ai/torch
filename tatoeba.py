#!/usr/bin/env python3
from os import listdir, makedirs
from os.path import isfile, getsize, isdir, getsize
from random import randint
from vocab import Vocab
from data import Dataframe

def convert(dataset):
    file = get_largest_file(dataset)
    inputs, targets = extract_sentence_pairs(file)
    vocab = Vocab(sentences=inputs + targets)
    inputs, targets = vocab.encode(inputs), vocab.encode(targets)
    data = Dataframe(inputs, targets)

    return vocab, data

def extract_sentence_pairs(file):
    inputs = []
    targets = []
    with open(file, "r") as f:
        for line in f:
            splits = line.split("\t")
            if len(splits) >= 2:
                inputs.append(splits[0])
                targets.append(splits[1])
    return inputs, targets


def get_largest_file(folder):
    largest_size = 0
    largest_file = ""
    for file in listdir(folder):
        if isfile(f"{folder}/{file}"):
            size = getsize(f"{folder}/{file}")
            if size > largest_size:
                largest_size = size
                largest_file = f"{folder}/{file}"
    return largest_file


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("dataset", help="path to the cornell movie-dialogs corpus, downlaoded from http://www.manythings.org/anki/")
    parser.add_argument("--outpath", default="tmp/data", help="the folder to save the output")
    parser.add_argument("--split_percentage", default=0.1, help="the percentage to split for validation")
    args = parser.parse_args()

    if not isdir(args.outpath):
        makedirs(args.outpath)

    vocab, data = convert(args.dataset)
    print(f"Created vocab with {len(vocab)} entries")

    total_size = min(len(data.src), len(data.tgt))
    split_size = int(total_size * args.split_percentage)

    valid_inputs =[]
    valid_targets = []
    for n in range(split_size):
        ind = randint(0, total_size-n)
        valid_inputs.append(data.src.pop(ind))
        valid_targets.append(data.tgt.pop(ind))

    valid_data = data = Dataframe(valid_inputs, valid_targets)

    print(f"Created train dataset with {total_size-split_size} sentence pairs")
    print(f"Created valid dataset with {split_size} sentence pairs")

    vocab.save(f"{args.outpath}/vocab.json")
    data.save(f"{args.outpath}/train.json")
    valid_data.save(f"{args.outpath}/valid.json")
