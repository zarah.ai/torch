#!/usr/bin/env python3
import sys, os

def print_bar(n, total, suffix="", length=25, previous_length=0):
    iteration = "{0:0{1}d}".format(n, len(f"{total}"))
    rows, columns = os.popen('stty size', 'r').read().split()
    filled_length = int(length * n // total)
    bar = ">" + " " * (length-1)
    bar = "-" * filled_length + bar[:length-filled_length]

    if suffix == "":
        perc = int(round((n / total) * 100))
        suffix = f"{perc}%"

    line = f"{iteration}/{total} [{bar}] - {suffix}"

    line = line + " " * max(0, previous_length - len(line))
    print(line, end="\r")
    if n == total:
        print()
    return len(line)

if __name__ == "__main__":
    import argparse
    import time

    parser = argparse.ArgumentParser()
    parser.add_argument("--iterations", default=100, type=int)
    args = vars(parser.parse_args())

    print("Printing test progressbar")

    iterations = args["iterations"]

    line_length = print_bar(0, iterations)
    for n in range(iterations):
        time.sleep(0.1)
        line_length = print_bar(n+1, iterations, previous_length=line_length)
