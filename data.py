#!/usr/bin/env python3
from json import load, dump
from torch import tensor, randperm
from vocab import Vocab, load_vocab
from torch import is_tensor, ones, triu
from torch.utils.data import Dataset, DataLoader

class Dataiter(DataLoader):
    def __init__(self, dataset, batch_size, shuffle):
        super(Dataiter, self).__init__(dataset, batch_size=1, shuffle=False, num_workers=4, pin_memory=True)
        self.iter = iter(self)
        self.batc = batch_size
        self.shuf = shuffle

    def next(self, device="cpu"):
        try:
            next_batch = self.iter.next()
        except StopIteration:
            if self.shuf:
                self.dataset._gen_batches(self.batc, True)
            self.iter = iter(self)
            next_batch = self.iter.next()
        next_batch[0] = next_batch[0].to(device).squeeze(0).transpose(0, 1)
        next_batch[1] = next_batch[1].to(device).squeeze(0)
        next_batch[2] = next_batch[2].to(device).squeeze(0).transpose(0, 1)
        next_batch[3] = next_batch[3].to(device).squeeze(0)
        next_batch[4] = next_batch[4].to(device).squeeze(0)
        return next_batch

class Dataframe(Dataset):
    def __init__(self, src, tgt):
        super(Dataframe, self).__init__()
        self.src = src
        self.tgt = tgt

    def __len__(self):
        return len(self.ind)

    def __getitem__(self, i):
        if is_tensor(i):
            i = i.tolist()
        batch_ind = self.ind[i]

        src_raw = [ self.src[i] for i in batch_ind ]
        src_max = len(max(src_raw, key=len))
        src_fin = [ x + [0] * (src_max - len(x)) for x in src_raw ]
        src_ten = tensor(src_fin)
        src_msk = src_ten == 0

        tgt_raw = [ [1] + self.tgt[i] + [2] for i in batch_ind ]
        tgt_max = len(max(tgt_raw, key=len))
        tgt_fin = [ x + [0] * (tgt_max - len(x)) for x in tgt_raw ]
        tgt_ten = tensor(tgt_fin)
        tgt_msk = tgt_ten == 0

        nopeek = tgt_ten.size()[-1]
        nopeek = ones(nopeek, nopeek)
        nopeek = triu(nopeek).transpose(0, 1)
        nopeek = nopeek.masked_fill(nopeek == 0, float('-inf')).masked_fill(nopeek == 1, float(0.0))


        return src_ten, src_msk, tgt_ten, tgt_msk, nopeek


    def _gen_batches(self, batch_size, shuffle=True):
        total_size = min(len(self.src), len(self.tgt))
        nbatches = total_size // batch_size
        perf_size = nbatches * batch_size
        if shuffle:
            ind = randperm(perf_size)
        else:
            ind = tesnor(range(perf_size))
        self.ind = ind.reshape(nbatches, batch_size)

    def sampler(self, batch_size, shuffle=True):
        self._gen_batches(batch_size, shuffle)
        return Dataiter(self, batch_size, shuffle)

    def save(self, filename):
        with open(filename, "w+") as f:
            dump({"src": self.src, "tgt": self.tgt }, f)

def load_dataframe(filename):
    with open(filename, "r") as f:
        pairs = load(f)
    return Dataframe(pairs["src"], pairs["tgt"])

if __name__ == "__main__":
    from argparse import ArgumentParser
    from torch.utils.data import DataLoader

    parser = ArgumentParser()
    parser.add_argument("--dataset", default="tmp/data/data.json", help="path to the dataset folder")
    parser.add_argument("--batch-size", default=8, help="the batch size")
    parser.add_argument("--shuffle", action="store_true", help="whether or not to shuffle the dataset")
    parser.add_argument("--n", default=5, help="the amount of batches to print")
    args = parser.parse_args()

    dataset = load_dataframe(args.dataset).repeat(batch_size=args.batch_size, shuffle=args.shuffle)

    for n in range(args.n):
        batch = dataset.next()
        print(f"src: {batch[0].size()}, src_mask: {batch[1].size()}, tgt: {batch[2].size()}, tgt_mask: {batch[3].size()}")
