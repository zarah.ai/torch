#!/usr/bin/env python3
from __future__ import absolute_import, division, print_function, unicode_literals
from os.path import isdir, dirname
from os import makedirs
from codecs import decode
from csv import writer
from random import randint
from re import compile
from data import Dataframe
from vocab import Vocab

def convert(dataset):
    lines_field = ["lineID", "characterID", "movieID", "character", "text"]
    conv_fields = ["character1ID", "character2ID", "movieID", "utteranceIDs"]

    lines = load_lines(f"{dataset}/movie_lines.txt", lines_field)
    conversations = load_conversations(f"{dataset}/movie_conversations.txt", lines, conv_fields)

    inputs, targets = extract_sentence_pairs(conversations)
    vocab = Vocab(sentences=inputs + targets)
    inputs, targets = vocab.encode(inputs), vocab.encode(targets)
    data = Dataframe(inputs, targets)

    return vocab, data

def load_lines(file_name, fields):
    lines = {}
    with open(file_name, 'r', encoding='iso-8859-1') as f:
        for line in f:
            values = line.split(" +++$+++ ")
            line_obj = {}
            for i, field in enumerate(fields):
                line_obj[field] = values[i]
            lines[line_obj['lineID']] = line_obj
    return lines

def load_conversations(file_name, lines, fields):
    conversations = []
    with open(file_name, 'r', encoding='iso-8859-1') as f:
        for line in f:
            values = line.split(" +++$+++ ")
            conv_obj = {}
            for i, field in enumerate(fields):
                conv_obj[field] = values[i]
            utterance_id_pattern = compile('L[0-9]+')
            line_ids = utterance_id_pattern.findall(conv_obj["utteranceIDs"])
            conv_obj["lines"] = []
            for line_id in line_ids:
                conv_obj["lines"].append(lines[line_id])
            conversations.append(conv_obj)
    return conversations


def extract_sentence_pairs(conversations):
    inputs = []
    targets = []
    for conversation in conversations:
        for i in range(len(conversation["lines"]) - 1):
            input_line = conversation["lines"][i]["text"].strip()
            target_line = conversation["lines"][i+1]["text"].strip()
            if input_line and target_line and len(input_line) <= 50 and len(target_line) <= 50:
                inputs.append(input_line)
                targets.append(target_line)
    return inputs, targets


if __name__ == "__main__":
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument("dataset", help="path to the cornell movie-dialogs corpus, downlaoded from https://www.cs.cornell.edu/~cristian/Cornell_Movie-Dialogs_Corpus.html")
    parser.add_argument("--outpath", default="tmp/data", help="the folder to save the output")
    parser.add_argument("--split_percentage", default=0.1, help="the percentage to split for validation")
    args = parser.parse_args()

    if not isdir(args.outpath):
        makedirs(args.outpath)

    vocab, data = convert(args.dataset)
    print(f"Created vocab with {len(vocab)} entries")

    total_size = min(len(data.src), len(data.tgt))
    split_size = int(total_size * args.split_percentage)

    valid_inputs = []
    valid_targets = []
    for n in range(split_size):
        ind = randint(0, total_size-n)
        valid_inputs.append(data.src.pop(ind))
        valid_targets.append(data.tgt.pop(ind))

    valid_data = data = Dataframe(valid_inputs, valid_targets)

    print(f"Created train dataset with {total_size-split_size} sentence pairs")
    print(f"Created valid dataset with {split_size} sentence pairs")

    vocab.save(f"{args.outpath}/vocab.json")
    data.save(f"{args.outpath}/train.json")
    valid_data.save(f"{args.outpath}/valid.json")
